# Parcel collector project

Parcel collector algorithm is implemented in Java. 

As a main solution is used a dynamic programming algorithm, but it also contains brute-force implementation.



## Installation

```bash
./gradlew jar
```
Add as a dependency to your project skeleton_java-1.0-SNAPSHOT.jar

## Usage

```java
import com.mobiquity.packer.Packer;

class Test {
    public static void main(String[] args) {
        Packer.pack("pathToFile"); // it will return String with items' indexes
    }
}
```

## Test

To run unit tests please use:
```bash
./gradlew test
```
Test coverage report is located here `build/jacoco-report/test/html/index.html`