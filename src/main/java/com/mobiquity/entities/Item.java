package com.mobiquity.entities;

import java.math.BigDecimal;

public class Item {
    public static final double MAX_ITEM_WEIGHT = 100;
    public static final BigDecimal MAX_ITEM_COST = new BigDecimal(100);

    private final int index;
    private final double weight;
    private final BigDecimal cost;

    public Item(int index, double weight, BigDecimal cost) {
        this.index = index;
        this.weight = weight;
        this.cost = cost;
    }

    public int getIndex() {
        return index;
    }

    public double getWeight() {
        return weight;
    }

    public BigDecimal getCost() {
        return cost;
    }
}
