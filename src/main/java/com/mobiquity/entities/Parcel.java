package com.mobiquity.entities;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Parcel {
    public static final double MAX_WEIGHT_LIMIT = 100;

    private final double maxWeight;
    private List<Item> packagedItems = new ArrayList<>();

    public Parcel(double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public double getMaxWeight() {
        return maxWeight;
    }

    public List<Item> getPackagedItems() {
        return new ArrayList<>(packagedItems);
    }

    public void setPackagedItems(List<Item> packagedItems) {
        this.packagedItems = new ArrayList<>(packagedItems);
        this.packagedItems.sort(Comparator.comparing(Item::getIndex));
    }
}
