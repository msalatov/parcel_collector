package com.mobiquity.exception;

import com.mobiquity.entities.Item;
import com.mobiquity.entities.Parcel;
import com.mobiquity.task_data.TaskInputData;

public class APIException extends Exception {

  public APIException(String message, Exception e) {
    super(message, e);
  }

  public APIException(String message) {
    super(message);
  }

  public static APIException nonExistingFileException(String path, Exception e) {
    return new APIException(String.format("File with path '%s' doesn't exist", path), e);
  }

  public static APIException emptyFileException(String path) {
    return new APIException(String.format("File with path '%s' is empty", path));
  }

  public static APIException wrongParcelWeightFormatException(String parcelWeight, Exception e) {
    return new APIException(String.format("Parcel weight '%s' is in the wrong format", parcelWeight), e);
  }

  public static APIException wrongItemIndexFormatException(String itemIndex, Exception e) {
    return new APIException(String.format("Item index '%s' is in the wrong format", itemIndex), e);
  }

  public static APIException wrongItemWeightFormatException(String itemWeight, Exception e) {
    return new APIException(String.format("Item weight '%s' is in the wrong format", itemWeight), e);
  }

  public static APIException wrongItemCostFormatException(String itemCost, Exception e) {
    return new APIException(String.format("Item cost '%s' is in the wrong format", itemCost), e);
  }

  public static APIException maxParcelWeightIsBiggerThanLimitException(double parcelWeight) {
    return new APIException(String.format("Max parcel weight '%.2f' is bigger than limit '%.2f'", parcelWeight, Parcel.MAX_WEIGHT_LIMIT));
  }

  public static APIException maxParcelWeightIsLessOrEqual0Exception(double parcelWeight) {
    return new APIException(String.format("Max parcel weight '%.2f' is less or equal 0", parcelWeight));
  }

  public static APIException tooManyItemsToChooseFromException(String task) {
    return new APIException(String.format("Task '%s' contains more than %d items to choose from", task, TaskInputData.MAX_AMOUNT_OF_ITEMS));
  }

  public static APIException itemWeightIsBiggerThanLimitException(double itemWeight) {
    return new APIException(String.format("Item weight '%.2f' is bigger than limit '%.2f'", itemWeight, Item.MAX_ITEM_WEIGHT));
  }

  public static APIException itemWeightIsLessOrEqual0Exception(double itemWeight) {
    return new APIException(String.format("Item weight '%.2f' is less or equal 0", itemWeight));
  }

  public static APIException itemCostIsBiggerThanLimitException(String itemCost) {
    return new APIException(String.format("Item cost '%s' is bigger than limit '%.2f'", itemCost, Item.MAX_ITEM_COST));
  }

  public static APIException itemCostIsLessOrEqual0Exception(String itemCost) {
    return new APIException(String.format("Item cost '%s' is less or equal 0", itemCost));
  }

  public static APIException itemIndexIsLessOrEqual0(String itemIndex) {
    return new APIException(String.format("Item index '%s' is less or equal 0", itemIndex));
  }

  public static APIException itemIndexHasDuplicate(int index) {
    return new APIException(String.format("Item index '%d' is duplicated", index));
  }
}
