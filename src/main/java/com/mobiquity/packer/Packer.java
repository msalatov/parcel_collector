package com.mobiquity.packer;

import com.mobiquity.exception.APIException;
import com.mobiquity.parcel_collector.ParcelCollector;
import com.mobiquity.parser.FileParser;
import com.mobiquity.presenter.PresenterIndexesAsString;

public class Packer {

  private Packer() {
  }

  public static String pack(String filePath) throws APIException {
    var fileParser = new FileParser();
    var presenter = new PresenterIndexesAsString();
    var taskInputDataList = fileParser.parseInput(filePath);
    var parcels = ParcelCollector.collectSeveralParcels(taskInputDataList);
    return presenter.presentAsString(parcels);
  }
}
