package com.mobiquity.parcel_collector;

import com.mobiquity.entities.Item;
import com.mobiquity.entities.Parcel;
import com.mobiquity.task_data.TaskInputData;

import java.util.ArrayList;
import java.util.List;

public class AltParcelCollector {
    List<Item> items;
    double maxWeight;
    int amountOfItems;
    CostAndWeight bestSoFar;
    boolean[] currentSet;
    Parcel parcel;

    public AltParcelCollector(TaskInputData inputData) {
        this.items = inputData.getItems();
        this.maxWeight = inputData.getParcel().getMaxWeight();
        this.amountOfItems = inputData.getItems().size();
        this.bestSoFar = new CostAndWeight(-1, -1);
        this.currentSet = new boolean[amountOfItems];
        this.parcel = inputData.getParcel();
    }

    public Parcel collectParcel() {
        solve(amountOfItems - 1);
        return parcel;
    }

    private void solve(int index) {
        if (index < 0) {
            double weight = 0;
            double cost = 0;
            for (int i = 0; i < amountOfItems; i++) {
                if (currentSet[i]) {
                    weight += items.get(i).getWeight();
                    cost += items.get(i).getCost().doubleValue();
                }
            }
            var currentCAW = new CostAndWeight(cost, weight);
            if (weight <= maxWeight && currentCAW.compareTo(bestSoFar) > 0) {
                bestSoFar = currentCAW;
                saveSolution();
            }
            return;
        }

        currentSet[index] = true;
        solve(index - 1);

        currentSet[index] = false;
        solve(index - 1);
    }

    private void saveSolution() {
        var itemsToPackage = new ArrayList<Item>();
        for (int i = 0; i < amountOfItems; i++) {
            if (currentSet[i]) itemsToPackage.add(items.get(i));
        }
        parcel.setPackagedItems(itemsToPackage);
    }
}
