package com.mobiquity.parcel_collector;

import static com.mobiquity.utils.MathHelper.THRESHOLD;

class CostAndWeight implements Comparable<CostAndWeight> {
    double cost;
    double weight;
    CostAndWeight(double cost, double weight) {
        this.cost = cost;
        this.weight = weight;
    }

    @Override
    public int compareTo(CostAndWeight caw2) {
        if (this.cost - caw2.cost > THRESHOLD) return 1;
        else if (caw2.cost - this.cost > THRESHOLD) return -1;
        else if (caw2.weight - this.weight > THRESHOLD) return 1;
        else if (this.weight - caw2.weight > THRESHOLD) return -1;
        else return 0;
    }
}
