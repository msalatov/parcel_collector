package com.mobiquity.parcel_collector;

import com.mobiquity.entities.Item;
import com.mobiquity.entities.Parcel;
import com.mobiquity.task_data.TaskInputData;

import java.util.ArrayList;
import java.util.List;

import static com.mobiquity.utils.MathHelper.firstBiggerOrEqualSecond;

public class ParcelCollector {

    public static Parcel collectParcel(TaskInputData inputData) {
        var packagedItems = new ArrayList<Item>();

        var items = inputData.getItems();
        int amountOfItems = items.size();
        double maxWeight = inputData.getParcel().getMaxWeight();
        boolean hasDecimalPart = maxWeight % 1 == 0;
        int size = (int)maxWeight + (hasDecimalPart ? 2 :  1);
        CostAndWeight[][] matrix = new CostAndWeight[amountOfItems + 1][size];

        for (int i = 0; i <= amountOfItems; i++) {
            for (int w = 0; w <= size - 1; w++) {
                double currentMaxWeight = (w == size - 1) ? maxWeight : w;

                if (i == 0 || w == 0) matrix[i][w] = new CostAndWeight(0, 0);
                else if (isEnoughSpaceForItem(currentMaxWeight, items.get(i - 1).getWeight())) {
                    double itemCost = items.get(i - 1).getCost().doubleValue();
                    double itemWeight = items.get(i - 1).getWeight();
                    int bestSuitableIndex = chooseIndex(currentMaxWeight, itemWeight, matrix[i - 1]);
                    double newSum = itemCost + matrix[i - 1][bestSuitableIndex].cost;
                    double newWeight = itemWeight + matrix[i - 1][bestSuitableIndex].weight;
                    var currentCAW = new CostAndWeight(newSum, newWeight);
                    var prevCAW = matrix[i - 1][w];
                    matrix[i][w] = (currentCAW.compareTo(prevCAW) > 0) ? currentCAW : prevCAW;
                } else matrix[i][w] = matrix[i - 1][w];
            }
        }

        int w = size - 1;
        double maxCost = matrix[amountOfItems][w].cost;
        for (int i = amountOfItems; i > 0 && maxCost > 0; i--) {
            if (matrix[i][w].compareTo(matrix[i - 1][w]) == 0) continue;
            else {
                double currentMaxWeight = (w == size - 1) ? maxWeight : w;
                var itemToAdd = items.get(i - 1);
                packagedItems.add(itemToAdd);
                maxCost = maxCost - itemToAdd.getCost().doubleValue();
                w = chooseIndex(currentMaxWeight, itemToAdd.getWeight(), matrix[i - 1]);
            }
        }

        inputData.getParcel().setPackagedItems(packagedItems);
        return inputData.getParcel();
    }

    public static List<Parcel> collectSeveralParcels(List<TaskInputData> tasksList) {
        var parcels = new ArrayList<Parcel>();
        for (var task : tasksList) {
            parcels.add(collectParcel(task));
        }
        return parcels;
    }

    private static boolean isEnoughSpaceForItem(double parcelCapacity, double itemWeight) {
        return firstBiggerOrEqualSecond(parcelCapacity, itemWeight);
    }

    private static int chooseIndex(double currentWeight, double itemWeight, CostAndWeight[] matrix) {
        double leftWeight = currentWeight - itemWeight;
        int lowIndex = (int)leftWeight;
        int topIndex = lowIndex + 1;
        return firstBiggerOrEqualSecond(leftWeight, matrix[topIndex].weight) ? topIndex : lowIndex;
    }
}
