package com.mobiquity.parser;

import com.mobiquity.entities.Item;
import com.mobiquity.entities.Parcel;
import com.mobiquity.exception.APIException;
import com.mobiquity.task_data.TaskInputData;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mobiquity.utils.MathHelper.firstBiggerOrEqualSecond;
import static com.mobiquity.utils.PriceConverter.convertItemCostToBigDecimal;

public class FileParser implements Parser {

    @Override
    public List<TaskInputData> parseInput(String filePath) throws APIException {
        List<TaskInputData> parsedInputData = new ArrayList<>();

        try {
            List<String> lines = Files.readAllLines(Paths.get(filePath));
            fileIsNotEmpty(filePath);
            for(var line : lines) {
                parsedInputData.add(parseTaskData(line));
            }
        } catch (IOException e) {
            throw APIException.nonExistingFileException(filePath, e);
        }

        return parsedInputData;
    }

    private void fileIsNotEmpty(String filePath) throws APIException {
        File file = new File(filePath);
        if (file.length() == 0) {
            throw APIException.emptyFileException(filePath);
        }
    }

    private TaskInputData parseTaskData(String line) throws APIException {
        double maxParcelWeight = parseMaxWeight(line);
        List<Item> taskItems = parseItems(line);
        return new TaskInputData(new Parcel(maxParcelWeight), taskItems);
    }

    private double parseMaxWeight(String line) throws APIException {
        String maxWeight = line.split(":")[0].trim();
        try {
            double maxParcelWeight = Double.parseDouble(maxWeight);
            if (firstBiggerOrEqualSecond(0, maxParcelWeight)) throw APIException.maxParcelWeightIsLessOrEqual0Exception(maxParcelWeight);
            if (!firstBiggerOrEqualSecond(Parcel.MAX_WEIGHT_LIMIT, maxParcelWeight)) throw APIException.maxParcelWeightIsBiggerThanLimitException(maxParcelWeight);
            return maxParcelWeight;
        } catch (NumberFormatException e) {
            throw APIException.wrongParcelWeightFormatException(maxWeight, e);
        }
    }

    private List<Item> parseItems(String line) throws APIException {
        List<Item> items = new ArrayList<>();
        var indexes = new HashSet<Integer>();
        Pattern itemRegex = Pattern.compile("\\((.*?)\\)");
        Matcher regexMatcher = itemRegex.matcher(line);
        while (regexMatcher.find()) {
            String item = regexMatcher.group(1);
            var newItem = parseItem(item);
            if (indexes.contains(newItem.getIndex())) throw APIException.itemIndexHasDuplicate(newItem.getIndex());
            else indexes.add(newItem.getIndex());
            items.add(newItem);
        }
        if (items.size() > 15) throw APIException.tooManyItemsToChooseFromException(line);
        return items;
    }

    private Item parseItem(String item) throws APIException {
        String[] itemAttributes = item.split(",");
        int index = parseItemIndex(itemAttributes[0]);
        double weight = parseItemWeight(itemAttributes[1]);
        BigDecimal cost = parseItemCost(itemAttributes[2]);
        return new Item(index, weight, cost);
    }

    private int parseItemIndex(String itemIndex) throws APIException {
        try {
            int index = Integer.parseInt(itemIndex);
            if (index <= 0) throw APIException.itemIndexIsLessOrEqual0(itemIndex);
            return index;
        } catch (NumberFormatException e) {
            throw APIException.wrongItemIndexFormatException(itemIndex, e);
        }
    }

    private double parseItemWeight(String itemWeight) throws APIException {
        try {
            double weight = Double.parseDouble(itemWeight);
            if (firstBiggerOrEqualSecond(0, weight)) throw APIException.itemWeightIsLessOrEqual0Exception(weight);
            if (!firstBiggerOrEqualSecond(Item.MAX_ITEM_WEIGHT, weight)) throw APIException.itemWeightIsBiggerThanLimitException(weight);
            return weight;
        } catch (NumberFormatException e) {
            throw APIException.wrongItemWeightFormatException(itemWeight, e);
        }
    }

    private BigDecimal parseItemCost(String itemCost) throws APIException {
        var cost = convertItemCostToBigDecimal(itemCost);
        if (cost.compareTo(new BigDecimal(0)) <= 0) throw APIException.itemCostIsLessOrEqual0Exception(itemCost);
        if (cost.compareTo(Item.MAX_ITEM_COST) > 0) throw APIException.itemCostIsBiggerThanLimitException(itemCost);
        return cost;
    }
}
