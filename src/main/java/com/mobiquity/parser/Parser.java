package com.mobiquity.parser;

import com.mobiquity.task_data.TaskInputData;

import java.util.List;

public interface Parser {
    List<TaskInputData> parseInput(String data) throws Exception;
}
