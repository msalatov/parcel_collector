package com.mobiquity.presenter;

import com.mobiquity.entities.Parcel;

import java.util.List;

public interface PresenterAsString {
    String presentAsString(Parcel parcel);
    String presentAsString(List<Parcel> parcels);
}
