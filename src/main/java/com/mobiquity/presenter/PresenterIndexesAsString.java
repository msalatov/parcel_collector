package com.mobiquity.presenter;

import com.mobiquity.entities.Parcel;

import java.util.List;
import java.util.stream.Collectors;

public class PresenterIndexesAsString implements PresenterAsString {
    @Override
    public String presentAsString(Parcel parcel) {
        String itemsIndexes = parcel.getPackagedItems().stream()
                .map(item -> String.valueOf(item.getIndex()))
                .collect(Collectors.joining(","));
        return itemsIndexes.equals("") ? "-" : itemsIndexes;
    }

    @Override
    public String presentAsString(List<Parcel> parcels) {
        return parcels.stream()
                .map(this::presentAsString)
                .collect(Collectors.joining("\n"));
    }
}
