package com.mobiquity.task_data;

import com.mobiquity.entities.Item;
import com.mobiquity.entities.Parcel;

import java.util.ArrayList;
import java.util.List;

public class TaskInputData {
    public static final int MAX_AMOUNT_OF_ITEMS = 15;

    private final Parcel parcel;
    private final List<Item> items;

    public TaskInputData(Parcel parcel, List<Item> items) {
        this.parcel = parcel;
        this.items = items;
    }

    public Parcel getParcel() {
        return parcel;
    }
    public List<Item> getItems() {
        return new ArrayList<>(items);
    }
}
