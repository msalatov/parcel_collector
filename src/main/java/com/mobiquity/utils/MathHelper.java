package com.mobiquity.utils;

public class MathHelper {
    public static final double THRESHOLD = 0.00001;

    public static boolean firstBiggerOrEqualSecond(double firstDouble, double secondDouble) {
        return firstDouble > secondDouble || Math.abs(firstDouble - secondDouble) < THRESHOLD;
    }
}
