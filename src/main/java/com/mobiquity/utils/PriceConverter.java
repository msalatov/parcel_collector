package com.mobiquity.utils;

import com.mobiquity.exception.APIException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;

public class PriceConverter {
    private PriceConverter() {}

    public static BigDecimal convertItemCostToBigDecimal(String itemCost) throws APIException {
        NumberFormat priceFormat = NumberFormat.getCurrencyInstance();
        priceFormat.setMaximumFractionDigits(2);
        Currency euro = Currency.getInstance("EUR");
        priceFormat.setCurrency(euro);
        try {
            Number number = priceFormat.parse(itemCost);
            return (new BigDecimal(number.toString())).setScale(2, RoundingMode.HALF_UP);
        } catch (ParseException e) {
            throw APIException.wrongItemCostFormatException(itemCost, e);
        }
    }
}
