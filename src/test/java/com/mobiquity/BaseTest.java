package com.mobiquity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BaseTest {
    protected String folderPath = "src/test/resources/";

    protected String getFilePath(String filename) {
        return folderPath + filename + ".txt";
    }

    protected void createFileWithNameAndContent(String filename, String content) {
        try {
            Files.writeString(Paths.get(folderPath + filename + ".txt"), content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
