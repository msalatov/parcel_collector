package com.mobiquity.packer;

import com.mobiquity.BaseTest;
import com.mobiquity.exception.APIException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PackerTest extends BaseTest {

    @Test
    public void collectParcelSuccessfully() throws IOException, APIException {
        var expected = Files.readString(Path.of(getFilePath("example_output")));
        var parcels = Packer.pack(getFilePath("example_input"));
        assertEquals(expected, parcels);
    }
}
