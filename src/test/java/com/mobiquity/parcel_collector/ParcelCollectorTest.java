package com.mobiquity.parcel_collector;

import com.mobiquity.BaseTest;
import com.mobiquity.entities.Parcel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import testdata.TaskDataProvider;

import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParcelCollectorTest extends BaseTest {

    TaskDataProvider taskDataProvider;

    @BeforeEach
    public void setUp() {
        taskDataProvider = new TaskDataProvider();
    }

    @Test
    public void correctParcelContentWithValidInput() {
        var parcel1 = ParcelCollector.collectParcel(taskDataProvider.TASK_DATA1);
        var parcel2 = ParcelCollector.collectParcel(taskDataProvider.TASK_DATA2);
        var parcel3 = ParcelCollector.collectParcel(taskDataProvider.TASK_DATA3);
        var parcel4 = ParcelCollector.collectParcel(taskDataProvider.TASK_DATA4);
        assertEquals("4", convertItemsToString(parcel1));
        assertEquals("", convertItemsToString(parcel2));
        assertEquals("2, 7", convertItemsToString(parcel3));
        assertEquals("8, 9", convertItemsToString(parcel4));
    }

    @Test
    public void collectParcelWhenParcelWeightIsDecimal() {
        var parcel = ParcelCollector.collectParcel(taskDataProvider.TASK_DATA_PARCEL_WEIGHT_DECIMAL);
        assertEquals("1, 5", convertItemsToString(parcel));
    }

    @Test
    public void collectParcelWhenSeveralEqualCosts() {
        var parcel = ParcelCollector.collectParcel(taskDataProvider.TASK_DATA_SEVERAL_EQUAL_COSTS);
        assertEquals("5, 6", convertItemsToString(parcel));
    }

    @Test
    public void correctParcelContentWithValidInputAlt() {
        var parcel1 = new AltParcelCollector(taskDataProvider.TASK_DATA1).collectParcel();
        var parcel2 = new AltParcelCollector(taskDataProvider.TASK_DATA2).collectParcel();
        var parcel3 = new AltParcelCollector(taskDataProvider.TASK_DATA3).collectParcel();
        var parcel4 = new AltParcelCollector(taskDataProvider.TASK_DATA4).collectParcel();
        assertEquals("4", convertItemsToString(parcel1));
        assertEquals("", convertItemsToString(parcel2));
        assertEquals("2, 7", convertItemsToString(parcel3));
        assertEquals("8, 9", convertItemsToString(parcel4));
    }

    private String convertItemsToString(Parcel parcel) {
        return parcel.getPackagedItems().stream()
                .map(item -> String.valueOf(item.getIndex()))
                .collect(Collectors.joining(", "));
    }
}
