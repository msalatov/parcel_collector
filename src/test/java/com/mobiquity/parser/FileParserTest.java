package com.mobiquity.parser;

import com.mobiquity.BaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import testdata.TaskDataProvider;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileParserTest extends BaseTest {

    Parser parser;
    TaskDataProvider taskDataProvider;

    @BeforeEach
    public void setUp() {
        parser = new FileParser();
        taskDataProvider = new TaskDataProvider();
    }

    @AfterEach
    public void tearDown(TestInfo testInfo) {
        String testName = testInfo.getDisplayName().replace("()", "");
        try {
            Files.deleteIfExists(Paths.get(folderPath + testName + ".txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
