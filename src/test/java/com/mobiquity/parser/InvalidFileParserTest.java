package com.mobiquity.parser;

import com.mobiquity.exception.APIException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InvalidFileParserTest extends FileParserTest {

    @Test
    public void fileDoesNotExist() {
        String nonExistentFile = getFilePath("fileDoesNotExist");
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(nonExistentFile));
        String expectedMessage = String.format("File with path '%s' doesn't exist", nonExistentFile);
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void emptyFile() {
        String filename = "emptyFile";
        createFileWithNameAndContent(filename, "");
        String emptyFile = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(emptyFile));
        String expectedMessage = String.format("File with path '%s' is empty", emptyFile);
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}
