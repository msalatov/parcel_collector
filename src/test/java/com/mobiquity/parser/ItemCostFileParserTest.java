package com.mobiquity.parser;

import com.mobiquity.entities.Item;
import com.mobiquity.exception.APIException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ItemCostFileParserTest extends FileParserTest {

    @Test
    public void wrongItemCostFormat() {
        String filename = "wrongItemCostFormat";
        createFileWithNameAndContent(filename, "76 : (1,53.38,€45) (2,88.62,someCost) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String invalidItemCost = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(invalidItemCost));
        String expectedMessage = "Item cost 'someCost' is in the wrong format";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void itemCostIsGreaterThanLimit() {
        String filename = "itemCostIsGreaterThanLimit";
        createFileWithNameAndContent(filename, "76 : (1,53.38,€45) (2,88.62,€342) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String itemCostIsGreaterThanLimit = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(itemCostIsGreaterThanLimit));
        String expectedMessage = String.format("Item cost '€342' is bigger than limit '%.2f'", Item.MAX_ITEM_COST);
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void itemCostIsEqual0() {
        String filename = "itemCostIsEqual0";
        createFileWithNameAndContent(filename, "15 : (1,15.43,€0) (2,88.62,€98) (3,1.00,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String itemCostIsEqual0 = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(itemCostIsEqual0));
        String expectedMessage = "Item cost '€0' is less or equal 0";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void itemCostIsLessThan0() {
        String filename = "itemCostIsLessThan0";
        createFileWithNameAndContent(filename, "15 : (1,15.43,-€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String itemCostIsLessThan0 = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(itemCostIsLessThan0));
        String expectedMessage = "Item cost '-€45' is less or equal 0";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}
