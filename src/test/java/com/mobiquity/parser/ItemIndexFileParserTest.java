package com.mobiquity.parser;

import com.mobiquity.exception.APIException;
import com.mobiquity.task_data.TaskInputData;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ItemIndexFileParserTest extends FileParserTest {

    @Test
    public void wrongItemIndexFormat() {
        String filename = "wrongItemIndexFormat";
        createFileWithNameAndContent(filename, "65 : (1,53.38,€45) (2,88.62,€98) (index,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String invalidItemIndex = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(invalidItemIndex));
        String expectedMessage = "Item index 'index' is in the wrong format";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void taskWithMoreThan15Items() {
        String filename = "taskWithMoreThan15Items";
        String task = "99 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48) (7,53.38,€45) (8,88.62,€98) (9,78.48,€3) (10,72.30,€76) (11,30.18,€9) (12,46.34,€48) (13,78.48,€3) (14,72.30,€76) (15,30.18,€9) (16,46.34,€48)";
        createFileWithNameAndContent(filename, task);
        String taskWithMoreThan15Items = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(taskWithMoreThan15Items));
        String expectedMessage = String.format("Task '%s' contains more than %d items to choose from", task, TaskInputData.MAX_AMOUNT_OF_ITEMS);
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void itemIndexIsLessThan0() {
        String filename = "itemIndexIsLessThan0";
        String task = "99 : (-1,53.38,€45) (2,88.62,€98) (3,78.48,€3)";
        createFileWithNameAndContent(filename, task);
        String itemIndexIsLessThan0 = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(itemIndexIsLessThan0));
        String expectedMessage = "Item index '-1' is less or equal 0";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void itemIndexIsEqual0() {
        String filename = "itemIndexIsEqual0";
        String task = "99 : (1,53.38,€45) (0,88.62,€98) (3,78.48,€3)";
        createFileWithNameAndContent(filename, task);
        String itemIndexIsEqual0 = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(itemIndexIsEqual0));
        String expectedMessage = "Item index '0' is less or equal 0";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void itemIndexIsDuplicated() {
        String filename = "itemIndexIsDuplicated";
        String task = "99 : (1,53.38,€45) (1,88.62,€98) (3,78.48,€3)";
        createFileWithNameAndContent(filename, task);
        String itemIndexIsDuplicated = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(itemIndexIsDuplicated));
        String expectedMessage = "Item index '1' is duplicated";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}
