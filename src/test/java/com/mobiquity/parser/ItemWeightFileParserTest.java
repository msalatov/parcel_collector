package com.mobiquity.parser;

import com.mobiquity.entities.Item;
import com.mobiquity.exception.APIException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ItemWeightFileParserTest extends FileParserTest {

    @Test
    public void wrongItemWeightFormat() {
        String filename = "wrongItemWeightFormat";
        createFileWithNameAndContent(filename, "35 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,itemWeight,€76) (5,30.18,€9) (6,46.34,€48)");
        String invalidItemWeight = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(invalidItemWeight));
        String expectedMessage = "Item weight 'itemWeight' is in the wrong format";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void itemWeightIsGreaterThanLimit() {
        String filename = "itemWeightIsGreaterThanLimit";
        createFileWithNameAndContent(filename, "35 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,123.32,€76) (5,30.18,€9) (6,46.34,€48)");
        String itemWeightIsGreaterThanLimit = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(itemWeightIsGreaterThanLimit));
        String expectedMessage = String.format("Item weight '123.32' is bigger than limit '%.2f'", Item.MAX_ITEM_WEIGHT);
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void itemWeightIsEqual0() {
        String filename = "itemWeightIsEqual0";
        createFileWithNameAndContent(filename, "15 : (1,15.43,€45) (2,88.62,€98) (3,0.00,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String itemWeightIsEqual0 = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(itemWeightIsEqual0));
        String expectedMessage = "Item weight '0.00' is less or equal 0";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void itemWeightIsLessThan0() {
        String filename = "itemWeightIsLessThan0";
        createFileWithNameAndContent(filename, "15 : (1,-15.43,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String itemWeightIsLessThan0 = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(itemWeightIsLessThan0));
        String expectedMessage = "Item weight '-15.43' is less or equal 0";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}
