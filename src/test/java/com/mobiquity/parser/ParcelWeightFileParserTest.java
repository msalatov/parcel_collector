package com.mobiquity.parser;

import com.mobiquity.entities.Parcel;
import com.mobiquity.exception.APIException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParcelWeightFileParserTest extends FileParserTest {

    @Test
    public void wrongParcelWeightFormat() {
        String filename = "wrongParcelWeightFormat";
        createFileWithNameAndContent(filename, "weight : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String invalidParcelWeight = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(invalidParcelWeight));
        String expectedMessage = "Parcel weight 'weight' is in the wrong format";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void maxParcelWeightIsBiggerThanLimit() {
        String filename = "maxParcelWeightIsBiggerThanLimit";
        createFileWithNameAndContent(filename, "157 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String maxParcelWeightGreaterThanLimit = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(maxParcelWeightGreaterThanLimit));
        String expectedMessage = String.format("Max parcel weight '157.00' is bigger than limit '%.2f'", Parcel.MAX_WEIGHT_LIMIT);
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void maxParcelWeightIsLessThan0() {
        String filename = "maxParcelWeightIsLessThan0";
        createFileWithNameAndContent(filename, "-15 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String maxParcelWeightLessThan0 = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(maxParcelWeightLessThan0));
        String expectedMessage = "Max parcel weight '-15.00' is less or equal 0";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void maxParcelWeightIsEqual0() {
        String filename = "maxParcelWeightIsEqual0";
        createFileWithNameAndContent(filename, "0.0 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String maxParcelWeightIsEqual0 = getFilePath(filename);
        Exception exception = assertThrows(APIException.class, () -> parser.parseInput(maxParcelWeightIsEqual0));
        String expectedMessage = "Max parcel weight '0.00' is less or equal 0";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void maxParcelWeightCanBeDecimal() throws Exception {
        String filename = "maxParcelWeightCanBeDecimal";
        createFileWithNameAndContent(filename, "87.5 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)");
        String maxParcelWeightDecimal = getFilePath(filename);
        double maxWeight = parser.parseInput(maxParcelWeightDecimal).get(0).getParcel().getMaxWeight();
        assertEquals(87.5, maxWeight);
    }
}
