package com.mobiquity.parser;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidFileParserTest extends FileParserTest {

    @Test
    public void validInputDataParsed() throws Exception {
        String validInputData = getFilePath("example_input");

        var parsedTasks = parser.parseInput(validInputData);
        for (int i = 0; i < parsedTasks.size(); i++) {
            var parsedTask = parsedTasks.get(i);
            var expectedTask = taskDataProvider.getAllTasksData()[i];
            var parsedItems = parsedTask.getItems();
            var expectedItems = expectedTask.getItems();

            assertEquals(expectedTask.getParcel().getMaxWeight(), parsedTask.getParcel().getMaxWeight());
            for (int x = 0; x < expectedItems.size(); x++) {
                var expectedItem = expectedItems.get(x);
                var parsedItem = parsedItems.get(x);
                assertEquals(expectedItem.getIndex(), parsedItem.getIndex());
                assertEquals(expectedItem.getWeight(), parsedItem.getWeight());
                assertEquals(expectedItem.getCost(), parsedItem.getCost());
            }
        }
    }
}
