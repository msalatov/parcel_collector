package com.mobiquity.presenter;

import com.mobiquity.BaseTest;
import com.mobiquity.entities.Item;
import com.mobiquity.entities.Parcel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import testdata.TaskDataProvider;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PresenterIndexesAsStringTest extends BaseTest {

    TaskDataProvider taskDataProvider;
    PresenterIndexesAsString presenter;

    @BeforeEach
    public void setUp() {
        taskDataProvider = new TaskDataProvider();
        presenter = new PresenterIndexesAsString();
    }

    @Test
    public void correctIndexesPresentationAsString() {
        var items = taskDataProvider.TASK_DATA1.getItems();
        var parcel = new Parcel(100);
        parcel.setPackagedItems(items);
        String presentation = presenter.presentAsString(parcel);
        assertEquals("1,2,3,4,5,6", presentation);
    }

    @Test
    public void correctEmptyIndexesPresentationAsString() {
        var items = new ArrayList<Item>();
        var parcel = new Parcel(100);
        parcel.setPackagedItems(items);
        String presentation = presenter.presentAsString(parcel);
        assertEquals("-", presentation);
    }

    @Test
    public void correctIndexesPresentationAsStringOfSeveralParcels() {
        var items1 = taskDataProvider.TASK_DATA1.getItems();
        var parcel1 = new Parcel(100);
        parcel1.setPackagedItems(items1);
        var items2 = new ArrayList<Item>();
        var parcel2 = new Parcel(100);
        parcel2.setPackagedItems(items2);
        var parcels = List.of(parcel1, parcel2);
        String presentation = presenter.presentAsString(parcels);
        assertEquals("1,2,3,4,5,6\n-", presentation);
    }
}
