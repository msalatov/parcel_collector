package testdata;

import com.mobiquity.entities.Item;
import com.mobiquity.entities.Parcel;
import com.mobiquity.task_data.TaskInputData;

import java.math.BigDecimal;
import java.util.List;

public class TaskDataProvider {

    public final TaskInputData TASK_DATA1 = new TaskInputData(
            new Parcel(81),
            List.of(
                    new Item(1,53.38, new BigDecimal("45.00")),
                    new Item(2,88.62, new BigDecimal("98.00")),
                    new Item(3,78.48, new BigDecimal("3.00")),
                    new Item(4,72.30, new BigDecimal("76.00")),
                    new Item(5,30.18, new BigDecimal("9.00")),
                    new Item(6,46.34, new BigDecimal("48.00"))
            )
    );
    public final TaskInputData TASK_DATA2 = new TaskInputData(
            new Parcel(8),
            List.of(
                    new Item(1,15.3, new BigDecimal("34.00"))
            )
    );
    public final TaskInputData TASK_DATA3 = new TaskInputData(
            new Parcel(75),
            List.of(
                    new Item(1,85.31, new BigDecimal("29.00")),
                    new Item(2,14.55, new BigDecimal("74.00")),
                    new Item(3,3.98, new BigDecimal("16.00")),
                    new Item(4,26.24, new BigDecimal("55.00")),
                    new Item(5,63.69, new BigDecimal("52.00")),
                    new Item(6,76.25, new BigDecimal("75.00")),
                    new Item(7,60.02, new BigDecimal("74.00")),
                    new Item(8,93.18, new BigDecimal("35.00")),
                    new Item(9,89.95, new BigDecimal("78.00"))
            )
    );
    public final TaskInputData TASK_DATA4 = new TaskInputData(
            new Parcel(56),
            List.of(
                    new Item(1,90.72, new BigDecimal("13.00")),
                    new Item(2,33.80, new BigDecimal("40.00")),
                    new Item(3,43.15, new BigDecimal("10.00")),
                    new Item(4,37.97, new BigDecimal("16.00")),
                    new Item(5,46.81, new BigDecimal("36.00")),
                    new Item(6,48.77, new BigDecimal("79.00")),
                    new Item(7,81.80, new BigDecimal("45.00")),
                    new Item(8,19.36, new BigDecimal("79.00")),
                    new Item(9,6.76, new BigDecimal("64.00"))
            )
    );

    public final TaskInputData TASK_DATA_PARCEL_WEIGHT_DECIMAL = new TaskInputData(
            new Parcel(81.5),
            List.of(
                    new Item(1,51.38, new BigDecimal("48.55")),
                    new Item(2,82.00, new BigDecimal("198.00")),
                    new Item(3,78.48, new BigDecimal("3.00")),
                    new Item(4,81.80, new BigDecimal("76.00")),
                    new Item(5,30.10, new BigDecimal("9.00")),
                    new Item(6,46.34, new BigDecimal("48.38"))
            )
    );

    public final TaskInputData TASK_DATA_SEVERAL_EQUAL_COSTS = new TaskInputData(
            new Parcel(81.5),
            List.of(
                    new Item(1,51.38, new BigDecimal("55.55")),
                    new Item(2,82.00, new BigDecimal("198.00")),
                    new Item(3,78.48, new BigDecimal("64.55")),
                    new Item(4,81.80, new BigDecimal("76.00")),
                    new Item(5,30.10, new BigDecimal("9.00")),
                    new Item(6,46.34, new BigDecimal("55.55"))
            )
    );

    public TaskInputData[] getAllTasksData() {
        return new TaskInputData[]{ TASK_DATA1, TASK_DATA2, TASK_DATA3, TASK_DATA4 };
    }
}
